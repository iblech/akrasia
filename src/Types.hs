module Types where

import Data.Time

data Goal = MkGoal
    { name   :: String
    , target :: Day -> Float
    , start  :: Day
    , end    :: Maybe Day
    }
