module Config where

import Data.Maybe
import Data.Time
import Types

goals :: [Goal]
goals = []

date :: String -> Day
date = fromJust . parseTimeM False defaultTimeLocale "%Y-%m-%d"
