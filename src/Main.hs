module Main where

import Control.Monad
import Control.Arrow ((***))
import Data.Maybe

import System.Environment
import System.IO
import Data.List
import Data.Time
import Text.Printf

import Types
import Config (goals)

main :: IO ()
main = do
    args <- getArgs
    case args of
        [] -> displayStatus
        [name] -> bumpGoal name
        _      -> putStrLn "Invalid execution."

bumpGoal :: String -> IO ()
bumpGoal name = do
    now     <- getCurrentTime
    current <- snd <$> last <$> readLog name
    let next = current + 1
    printf "Bump %s from %.0f to %.0f? " name current next
    hFlush stdout
    getLine
    appendFile (logFilename name) $ formatTime defaultTimeLocale "%s" now ++ " " ++ show next ++ "\n"

displayStatus :: IO ()
displayStatus = do
    now <- utctDay <$> getCurrentTime
    let currentGoals = filter (\g -> start g <= now && now <= maybe now id (end g)) goals
    putStrLn "goal\t\tstatus\tstreak\tis\tshould\tshould in one week"
    putStrLn "------------------------------------------------------------------"
    streaks <- forM currentGoals $ \g -> do
        log <- readLog (name g)
        let s = streak (start g) (target g) log now
        printf "%s\t\t%s\t%d days\t%.0f\t%.0f\t%.0f\n"
            (name g)
            (if isGoodDay (target g) log now then ":-)" else ":-(")
            s
            (snd . last $ log)
            (target g now)
            (target g $ addDays 7 now)
        return s
    putStrLn "------------------------------------------------------------------"
    printf "Total streak: %d days\n" (minimum streaks)

streak :: Day -> (Day -> Float) -> [(UTCTime,Float)] -> Day -> Int
streak start target history t =
    if head currentBlock == True
        then length currentBlock
        else 0
    where
    currentBlock = last $ group $ map (isGoodDay target history) [start..t]

isGoodDay :: (Day -> Float) -> [(UTCTime,Float)] -> Day -> Bool
isGoodDay target history t =
    let (t',x) = last $ filter (\(t',x) -> t' <= t) $ map (utctDay *** id) history
    in  x >= target t

readLog :: String -> IO [(UTCTime,Float)]
readLog name = map (parseLine . words) <$> lines <$> readFile (logFilename name)
    where
    parseLine [a,b] = (fromJust (parseTimeM False defaultTimeLocale "%s" a), read b)

logFilename :: String -> String
logFilename name = "logs/" ++ name ++ ".log"
